
�
android.location.motionstate!release_motion_state_feature_flaglocation"�Enables motion state feature designed to generate motion metrics stat on power consumption of various subsystems when devices are in motion vs not in-motion *	36017721908BQ
Kvendor/google/sensors/common/libsueznanoappclients/motionstate_flag.aconfigH P Zvendorb 
�
com.android.bluetooth.hal.flagspixel_bluetooth_hr_modepixel_bluetooth"Flag for HR Mode feature*	31766878708B3
-vendor/broadcom/bluetooth/flags/bthal.aconfigH P Zvendorb 
�
com.android.bluetooth.hal.flagspixel_bt_23p_aoc_offloadpixel_bluetooth"23P BT AOC Offload feature flag*	32346481008B3
-vendor/broadcom/bluetooth/flags/bthal.aconfigH P Zvendorb 
�
com.android.bluetooth.hal.flagspixel_bt_24f_aoc_offloadpixel_bluetooth"24F BT AOC Offload feature flag*	32346481008B3
-vendor/broadcom/bluetooth/flags/bthal.aconfigH P Zvendorb 
�
com.android.bluetooth.hal.flagspixel_bt_24m_aoc_offloadpixel_bluetooth"24M BT AOC Offload feature flag*	32346481008B3
-vendor/broadcom/bluetooth/flags/bthal.aconfigH P Zvendorb 
�
com.android.bluetooth.hal.flagspixel_bt_24p_aoc_offloadpixel_bluetooth"24P BT AOC Offload feature flag*	32346481008B3
-vendor/broadcom/bluetooth/flags/bthal.aconfigH P Zvendorb 
�
com.android.bluetooth.hal.flagspixel_bt_aoc_offload_efw_xportpixel_bluetooth")EFW-based AP<->AOC Transport feature flag*	34173173108B3
-vendor/broadcom/bluetooth/flags/bthal.aconfigH P Zvendorb 
�
com.android.bluetooth.hal.flags#pixel_bt_aoc_offload_reset_recoverypixel_bluetooth""AOC reset BT recovery feature flag*	34959855908B3
-vendor/broadcom/bluetooth/flags/bthal.aconfigH P Zvendorb
�
&com.google.android.input.twoshay.flags!enable_adaptive_touch_sensitivitysystem_sw_touch":Flag for enabling P24 feature: adaptive touch sensitivity.*	30515426908B/
)vendor/google/input/twoshay/flags.aconfigB�
�vendor/google/release/aconfig/ap3a/com.google.android.input.twoshay.flags/enable_adaptive_touch_sensitivity_flag_values.textprotoB�
�vendor/google/release/aconfig/ap4a/com.google.android.input.twoshay.flags/enable_adaptive_touch_sensitivity_flag_values.textprotoH P Zvendorb 
�
&com.google.android.input.twoshay.flagsenable_keyboard_modesystem_sw_touch"Enable keyboard mode feature.*	31112196908B/
)vendor/google/input/twoshay/flags.aconfigH P Zvendorb 
�
&com.google.android.input.twoshay.flagsenable_twoshay_utilities_modulesystem_sw_touch"Flag for enabling utilities.*	32411440808B/
)vendor/google/input/twoshay/flags.aconfigH P Zvendorb 
�
libgooglecamerahal.flagszsl_video_denoise_in_hwl
camera_hal":Enable HWL ZSL video processing and disable GCH processing*	34174849708BZ
Thardware/google/camera/common/hal/google_camera_hal/libgooglecamerahal_flags.aconfigH P Zvendorb 
�
lyric_flags.camera_provider%realtime_tpu_response_handling_thread
camera_hal"GEnables real-time priority for TPU response handling thread in DarwiNN.*	32291544208BX
Rvendor/google/services/LyricCameraHAL/src/lyric/core/camera_provider_flags.aconfigB�
zvendor/google/release/aconfig/ap3a/lyric_flags.camera_provider/realtime_tpu_response_handling_thread_flag_values.textprotoB�
zvendor/google/release/aconfig/ap4a/lyric_flags.camera_provider/realtime_tpu_response_handling_thread_flag_values.textprotoH P Zvendorb 
�
lyric_flags.color_spacedisplay_p3_preview
camera_hal"6Enables Display-P3 color space for HDR+ photo preview.*	30999155108BT
Nvendor/google/services/LyricCameraHAL/src/lyric/core/color_space_flags.aconfigBi
cvendor/google/release/aconfig/ap3a/lyric_flags.color_space/display_p3_preview_flag_values.textprotoBi
cvendor/google/release/aconfig/ap4a/lyric_flags.color_space/display_p3_preview_flag_values.textprotoH P Zvendorb 
�
lyric_flags.color_spacejpeg_r
camera_hal"Enables JPEG_R support.*	33657956008BT
Nvendor/google/services/LyricCameraHAL/src/lyric/core/color_space_flags.aconfigB]
Wvendor/google/release/aconfig/ap4a/lyric_flags.color_space/jpeg_r_flag_values.textprotoH P Zvendorb 
�
lyric_flags.dsnrenable_dsnr
camera_hal".Flag to enable DSNR feature on the lyric level*	30746086208BR
Lvendor/google/services/LyricCameraHAL/src/lyric/core/dsnr/dsnr_flags.aconfigB[
Uvendor/google/release/aconfig/ap3a/lyric_flags.dsnr/enable_dsnr_flag_values.textprotoB[
Uvendor/google/release/aconfig/ap4a/lyric_flags.dsnr/enable_dsnr_flag_values.textprotoH P Zvendorb 
�
lyric_flags.face_detectionuse_graph_runner_thread_pool
camera_hal"CAllows the graph runner pool to process the face detection requests*	31137818108Bf
`vendor/google/services/LyricCameraHAL/src/lyric/core/face_detection/face_detection_flags.aconfigBv
pvendor/google/release/aconfig/ap3a/lyric_flags.face_detection/use_graph_runner_thread_pool_flag_values.textprotoBv
pvendor/google/release/aconfig/ap4a/lyric_flags.face_detection/use_graph_runner_thread_pool_flag_values.textprotoH P Zvendorb 
�
lyric_flags.fefe_merged_event_thread
camera_hal"Enables merged FE event thread.*	32234869908BN
Hvendor/google/services/LyricCameraHAL/src/lyric/core/fe/fe_flags.aconfigBd
^vendor/google/release/aconfig/ap3a/lyric_flags.fe/fe_merged_event_thread_flag_values.textprotoBd
^vendor/google/release/aconfig/ap4a/lyric_flags.fe/fe_merged_event_thread_flag_values.textprotoH P Zvendorb 
�
lyric_flags.fe$realtime_fe_programming_event_thread
camera_hal";Enables real-time priority for FE programming event thread.*	31735425508BN
Hvendor/google/services/LyricCameraHAL/src/lyric/core/fe/fe_flags.aconfigBr
lvendor/google/release/aconfig/ap3a/lyric_flags.fe/realtime_fe_programming_event_thread_flag_values.textprotoBr
lvendor/google/release/aconfig/ap4a/lyric_flags.fe/realtime_fe_programming_event_thread_flag_values.textprotoH P Zvendorb 
�
lyric_flags.frontend_request#first_frame_async_buffer_allocation
camera_hal"hFlag to do async buffer allocation for the buffer allocation in frontend request node for the 1st frames*	31726873408Bj
dvendor/google/services/LyricCameraHAL/src/lyric/core/frontend_request/frontend_request_flags.aconfigB
yvendor/google/release/aconfig/ap3a/lyric_flags.frontend_request/first_frame_async_buffer_allocation_flag_values.textprotoB
yvendor/google/release/aconfig/ap4a/lyric_flags.frontend_request/first_frame_async_buffer_allocation_flag_values.textprotoH P Zvendorb 
�
lyric_flags.graphhal_touch_roi_tracking_enabled
camera_hal"9Flag to enable or disable HAL touch ROI tracking feature.*	32630552408BY
Svendor/google/services/LyricCameraHAL/src/lyric/core/top_level_graphs/graph.aconfigBo
ivendor/google/release/aconfig/ap4a/lyric_flags.graph/hal_touch_roi_tracking_enabled_flag_values.textprotoH P Zvendorb 
�
lyric_flags.graphpositive_offsets_enabled
camera_hal"2Flag to enable or disable positive offset feature.*	36834241708BY
Svendor/google/services/LyricCameraHAL/src/lyric/core/top_level_graphs/graph.aconfigH P Zvendorb 
�
lyric_flags.motion_sensorasync_query_enabled
camera_hal";Flag to enable asynchronous query of motion sensor samples.*	36715484708Bk
evendor/google/services/LyricCameraHAL/src/lyric/controllers/motion_sensor/motion_sensor_flags.aconfigH P Zvendorb 
�
lyric_flags.multicamcache_latest_frame_for_padding
camera_hal"AKeep the latest frame copy for padding in case the port is unused*	35495131208BZ
Tvendor/google/services/LyricCameraHAL/src/lyric/core/multicam/multicam_flags.aconfigH P Zvendorb 
�
lyric_flags.multicam$multicam_activation_resolver_enabled
camera_hal",Flag to enable Multicam Activation Resolver.*	30581084708BZ
Tvendor/google/services/LyricCameraHAL/src/lyric/core/multicam/multicam_flags.aconfigBx
rvendor/google/release/aconfig/ap3a/lyric_flags.multicam/multicam_activation_resolver_enabled_flag_values.textprotoBx
rvendor/google/release/aconfig/ap4a/lyric_flags.multicam/multicam_activation_resolver_enabled_flag_values.textprotoH P Zvendorb 
�
lyric_flags.multicam.multicam_in_market_activation_resolver_enabled
camera_hal"AFlag to enable Multicam Activation Resolver on in-market devices.*	34994492808BZ
Tvendor/google/services/LyricCameraHAL/src/lyric/core/multicam/multicam_flags.aconfigH P Zvendorb 
�
lyric_flags.page_releaseenabled
camera_hal"EEnables the PageReleaseController to control page releasing in scudo.*	34224973908Bi
cvendor/google/services/LyricCameraHAL/src/lyric/controllers/page_release/page_release_flags.aconfigB_
Yvendor/google/release/aconfig/ap4a/lyric_flags.page_release/enabled_flag_values.textprotoH P Zvendorb
�
lyric_flags.phase_detectionbma_conversion_uses_dsp
camera_hal"PEnable DSP acceleration for sparse block matching algorithm (BMA) preprocessing.*	33889225308Bh
bvendor/google/services/LyricCameraHAL/src/lyric/core/phase_detection/phase_detection_flags.aconfigBr
lvendor/google/release/aconfig/ap4a/lyric_flags.phase_detection/bma_conversion_uses_dsp_flag_values.textprotoH P Zvendorb 
�
lyric_flags.phase_detectionmlpd_uses_dsp
camera_hal"/Enable DSP acceleration for MLPD preprocessing.*	33889225508Bh
bvendor/google/services/LyricCameraHAL/src/lyric/core/phase_detection/phase_detection_flags.aconfigBh
bvendor/google/release/aconfig/ap4a/lyric_flags.phase_detection/mlpd_uses_dsp_flag_values.textprotoH P Zvendorb 
�
lyric_flags.placeholderplaceholder_first_flag
camera_hal"&Placeholder flag to test the workflow.*	30351601408BS
Mvendor/google/services/LyricCameraHAL/src/lyric_hwl/placeholder_flags.aconfigH P Zvendorb 
�
lyric_flags.placeholderplaceholder_second_flag
camera_hal".Placeholder flag to test simultaneous rollout.*	30351601408BS
Mvendor/google/services/LyricCameraHAL/src/lyric_hwl/placeholder_flags.aconfigH P Zvendorb 
�
lyric_flags.segmentationdsp_optimization_enabled
camera_hal"5Flag to enable dsp optimization when using the MCSSD.*	30820533808Bb
\vendor/google/services/LyricCameraHAL/src/lyric/core/segmentation/segmentation_flags.aconfigH P Zvendorb 
�
lyric_flags.segmentationmcssd_enabled
camera_hal"Flag to enable the MCSSD.*	30820533808Bb
\vendor/google/services/LyricCameraHAL/src/lyric/core/segmentation/segmentation_flags.aconfigH P Zvendorb 
�
lyric_flags.segmentationskin_mask_enabled
camera_hal""Flag to enable sending skin masks.*	30133705508Bb
\vendor/google/services/LyricCameraHAL/src/lyric/core/segmentation/segmentation_flags.aconfigBi
cvendor/google/release/aconfig/ap3a/lyric_flags.segmentation/skin_mask_enabled_flag_values.textprotoBi
cvendor/google/release/aconfig/ap4a/lyric_flags.segmentation/skin_mask_enabled_flag_values.textprotoH P Zvendorb 
�
lyric_flags.session_managerdestroy_analyzer_async
camera_hal"�Enables destroying the analyzer object in a separate thread, which makes CPA write to disk asynchronously and not blocking the camera close.*	31696164308BX
Rvendor/google/services/LyricCameraHAL/src/lyric/core/session_manager_flags.aconfigBq
kvendor/google/release/aconfig/ap3a/lyric_flags.session_manager/destroy_analyzer_async_flag_values.textprotoBq
kvendor/google/release/aconfig/ap4a/lyric_flags.session_manager/destroy_analyzer_async_flag_values.textprotoH P Zvendorb 
�
lyric_flags.sw_aligner_3penable_sw_aligner_3p
camera_hal"*Flag to enable SW Aligners on the 3p apps.*	30915858708Bw
qvendor/google/services/LyricCameraHAL/src/lyric/core/top_level_graphs/aligner_decider/sw_aligner_3p_flags.aconfigH P Zvendorb 
�
lyric_flags.teamfooduses_teamfood_config
camera_hal"=Indicates whether device is using Camera HAL teamfood config.*	31619796108BP
Jvendor/google/services/LyricCameraHAL/src/lyric_hwl/teamfood_flags.aconfigH P Zvendorb 
�
lyric_flags.thread_poolpersist_threads
camera_hal"6Persists graph runner threads between camera sessions.*	29845297908Bg
avendor/google/services/LyricCameraHAL/src/lyric/controllers/thread_pool/thread_pool_flags.aconfigH P Zvendorb
�
lyric_flags.three_a#input_slack_frame_alignment_enabled
camera_hal"5Use frame alignment on inputs with slack on 3A nodes.*	31847095208BX
Rvendor/google/services/LyricCameraHAL/src/lyric/core/three_a/three_a_flags.aconfigBv
pvendor/google/release/aconfig/ap3a/lyric_flags.three_a/input_slack_frame_alignment_enabled_flag_values.textprotoBv
pvendor/google/release/aconfig/ap4a/lyric_flags.three_a/input_slack_frame_alignment_enabled_flag_values.textprotoH P Zvendorb 
�
&vendor.google.battery_mitigation.flagsenable_modem_crash_mitigationbrownout_mitigation_modem"Control Modem crashing from BCL*	33038319308BY
Svendor/google/interfaces/battery_mitigation/configuration_flags/modem_flags.aconfigH P Zvendorb 
�
&vendor.google.battery_mitigation.flagsenable_swift_audio_mitigationbrownout_mitigation_audio"Control Swift_Audio_Mitigation*	32813955708BY
Svendor/google/interfaces/battery_mitigation/configuration_flags/audio_flags.aconfigB�
}vendor/google/release/aconfig/ap3a/vendor.google.battery_mitigation.flags/enable_swift_audio_mitigation_flag_values.textprotoB�
}vendor/google/release/aconfig/ap4a/vendor.google.battery_mitigation.flags/enable_swift_audio_mitigation_flag_values.textprotoH P Zvendorb 
�
#vendor.google.whitechapel.aoc.flagsenable_aocx_processingaoc"Enables aocx processing*	35431317908B=
7vendor/google/whitechapel/aoc/aoc_aconfig_flags.aconfigH P Zvendorb 
�
vendor.vibrator.hal.flagsremove_capovibrator"AThis flag controls the removal of utilizing Capo at the HAL level*	29022363008BB
<hardware/google/pixel/vibrator/cs40l26/VibratorFlags.aconfigB\
Vbuild/release/aconfig/ap3a/vendor.vibrator.hal.flags/remove_capo_flag_values.textprotoB\
Vbuild/release/aconfig/ap4a/vendor.vibrator.hal.flags/remove_capo_flag_values.textprotoB~
xvendor/google_devices/release/phones/pixel_2022/aconfig/ap4a/vendor.vibrator.hal.flags/remove_capo_flag_values.textprotoH PZvendorb 